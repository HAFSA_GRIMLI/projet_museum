package com.hafsa.myapplication.modellayer;

public class MuseumObjectDto {

    private String itemId;
    private String name;
    private String brand;
    private String description;
    private int year;
    private boolean working;

    public MuseumObjectDto(String itemId, String name, String brand, String description, int year, boolean working) {
        this.itemId = itemId;
        this.name = name;
        this.brand = brand;
        this.description = description;
        this.year = year;
        this.working = working;
    }

    // region setter and getter
    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean getWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }
    // endregion
}
