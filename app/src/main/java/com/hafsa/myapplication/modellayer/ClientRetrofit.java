package com.hafsa.myapplication.modellayer;

import com.hafsa.myapplication.domainlayer.MuseumObject;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.ArrayList;

public class ClientRetrofit {
    private static final String BASE_URL = "https://demo-lia.univ-avignon.fr/cerimuseum/";
    private static Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl(BASE_URL)
                                        .addConverterFactory(GsonConverterFactory.create())
                                        .build();

    private static Webservice ceriMuseumAPI;

    public static Webservice getCeriMuseumAPI() {
        if (ceriMuseumAPI == null) {
            ceriMuseumAPI = retrofit.create(Webservice.class);
        }
        return ceriMuseumAPI;
    }
}
