package com.hafsa.myapplication.modellayer;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import com.hafsa.myapplication.domainlayer.MuseumObject;

import java.util.List;

@Dao
public interface IMuseumObjectDao {
    @Insert
    void insertObject(MuseumObject museumObject);

    @Query("SELECT * FROM museumObject WHERE itemId = :id")
    List<MuseumObject> findProductById(String id);

    @Query("DELETE FROM museumObject WHERE itemId = :id")
    void deleteObject(String id);

    @Update
    void updateObject(MuseumObject museumObject);

    @Query("SELECT * FROM museumObject")
    LiveData<List<MuseumObject>> getAllObjects();
}
