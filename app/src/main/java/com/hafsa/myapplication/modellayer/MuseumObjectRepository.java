package com.hafsa.myapplication.modellayer;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.hafsa.myapplication.domainlayer.MuseumObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static com.hafsa.myapplication.modellayer.MuseumObjectRoomDatabase.databaseWriteExecutor;

public class MuseumObjectRepository {
    private MutableLiveData<List<MuseumObject>> searchResults = new MutableLiveData<>();

    private static final String TAG = "MuseumObjectRepository";
    private List<MuseumObject> museumObjectList = new ArrayList<>();
    private MutableLiveData<List<MuseumObject>> allObjects = new MutableLiveData<>() ;

    private IMuseumObjectDao museumObjectDao;

    private Webservice museumAPI = ClientRetrofit.getCeriMuseumAPI() ;



    public MuseumObjectRepository(Application application) {
        MuseumObjectRoomDatabase db = MuseumObjectRoomDatabase.getDatabase(application);
        museumObjectDao = db.museumObjectDao();
        museumObjectList = museumObjectDao.getAllObjects().getValue();

        allObjects.setValue(this.museumObjectList);
    }

    public void insertMuseumObject(MuseumObject museumObject) {
        databaseWriteExecutor.execute(() -> {
            museumObjectDao.insertObject(museumObject);
        });
    }

    public void deleteMuseumObject(String id) {
        databaseWriteExecutor.execute(() -> {
            museumObjectDao.deleteObject(id);
        });
    }

    public void readMuseumObject(String id) throws ExecutionException, InterruptedException {

        Future<List<MuseumObject>> fMuseumObjects = databaseWriteExecutor.submit(() -> {
            return museumObjectDao.findProductById(id);
        });
        try {
            searchResults.setValue(fMuseumObjects.get());
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public MutableLiveData<List<MuseumObject>> findObjectById(String id) {
//        // données de test
//        Map<String, MuseumObject> mock = new HashMap<String, MuseumObject>();
//        mock.put("test", new MuseumObject("test", "mac", "apple", "un vieux ordinateur plein de poussiere", 2008, false));
//        mock.put("test1", new MuseumObject("test1", "Iphone1", "apple", "un iphone très ancien", 2003, false));
//        mock.put("test2", new MuseumObject("test2", "Windows 95", "Microsoft", "un OS antique", 2015, true));
//        mock.put("test3", new MuseumObject("test3", "Google", "google", "description", 2001, false));
//        mock.put("test4", new MuseumObject("test4", "disquette", "IBM", "description", 2010, false));
//        mock.put("test5", new MuseumObject("test5", "hdd", "seagate", "description", 1990, false));
//        List<MuseumObject> res = new ArrayList<>();
//        res.add(mock.get(id));
//        searchResults.setValue(res);
        return searchResults;
    }

    public MutableLiveData<List<MuseumObject>> getAllObjects() {
        // données de test
//        List<MuseumObject> mock = new ArrayList<>();
//        //List<String> cat = new ArrayList<>(); cat.add("ordinateur"); cat.add("informatique");
//        mock.add(new MuseumObject("test", "mac", "apple", "un vieux ordinateur plein de poussiere", 2008, false));
//        //cat = new ArrayList<>(); cat.add("smartphone"); cat.add("informatique"); cat.add("telephonie");
//        mock.add(new MuseumObject("test1", "Iphone1", "apple", "un iphone très ancien", 2003, false));
//        //cat = new ArrayList<>(); cat.add("logiciel"); cat.add("OS"); cat.add("informatique");
//        mock.add(new MuseumObject("test2", "Windows 95", "Microsoft", "un OS antique", 2015, true));
//        //cat = new ArrayList<>(); cat.add("moteur de recherche"); cat.add("monopole"); cat.add("informatique"); cat.add("web");
//        mock.add(new MuseumObject("test3", "Google", "google", "description", 2001, false));
//        //cat = new ArrayList<>(); cat.add("support de stockage"); cat.add("petite capacité"); cat.add("informatique");
//        mock.add(new MuseumObject("test4", "disquette", "IBM", "description", 2010, false));
//        //cat = new ArrayList<>(); cat.add("support de stockage"); cat.add("grosse capacité"); cat.add("informatique");
//        mock.add(new MuseumObject("test5", "hdd", "seagate", "description", 1990, false));

        try {
            museumAPI.getMuseumObjectList().enqueue(new Callback<Map<String, MuseumObjectDto>>() {
                @Override
                public void onResponse(Call<Map<String, MuseumObjectDto>> call, Response<Map<String, MuseumObjectDto>> response) {
                    List<MuseumObject> museumObjectList = new ArrayList<>();
                    for (String item: response.body().keySet()) {
                        MuseumObjectDto museumObjectDto = response.body().get(item);
                        museumObjectDto.setItemId(item);
                        MuseumObject museumObject = new MuseumObject(museumObjectDto);
                        museumObjectList.add(museumObject);
                    }
                    allObjects.setValue(museumObjectList);
                }

                @Override
                public void onFailure(Call<Map<String, MuseumObjectDto>> call, Throwable t) {
                    Log.d(TAG, "onFailure: echec de récupération de la collection d'objet");
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return allObjects;
    }

}
