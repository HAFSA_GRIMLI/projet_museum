package com.hafsa.myapplication.modellayer;

import com.hafsa.myapplication.domainlayer.MuseumObject;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

import java.util.Map;

public interface Webservice {
    @Headers("Accept: application/json")
    @GET("collection")
    Call<Map<String, MuseumObjectDto>> getMuseumObjectList();

    @GET("items/{itemID}")
    MuseumObject getMuseumObject(@Path("itemID") String itemId);

}
