package com.hafsa.myapplication.modellayer;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.room.*;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import com.hafsa.myapplication.domainlayer.MuseumObject;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {MuseumObject.class}, version = 1, exportSchema = false)
public abstract class MuseumObjectRoomDatabase extends RoomDatabase {

    public abstract IMuseumObjectDao museumObjectDao();

    private static volatile MuseumObjectRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static MuseumObjectRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MuseumObjectRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), MuseumObjectRoomDatabase.class,
                            "museum_object_database").build();
                }
            }
        }
        return INSTANCE;
    }
}
