package com.hafsa.myapplication.domainlayer;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.hafsa.myapplication.modellayer.MuseumObjectRepository;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ObjectDetailViewModel extends AndroidViewModel {
    private MutableLiveData<MuseumObject> museumObjectsResult = new MutableLiveData<>();
    private MuseumObjectRepository repository ;

    public ObjectDetailViewModel(@NonNull Application application) {
        super(application);
        this.repository = new MuseumObjectRepository(application);
    }

    public LiveData<List<MuseumObject>> getResultMuseumObject(String id) {
        return repository.findObjectById(id);
    }
}
