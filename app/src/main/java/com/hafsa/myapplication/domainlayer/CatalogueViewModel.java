package com.hafsa.myapplication.domainlayer;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.hafsa.myapplication.modellayer.MuseumObjectRepository;

import java.util.List;

public class CatalogueViewModel extends AndroidViewModel {
    private MutableLiveData<MuseumObject> museumObjectsResult = new MutableLiveData<>();
    private MuseumObjectRepository repository ;

    public CatalogueViewModel(@NonNull Application application) {
        super(application);
        this.repository = new MuseumObjectRepository(application);
    }

    public LiveData<List<MuseumObject>> getResultMuseumObject() {
        return repository.getAllObjects();
    }


}
