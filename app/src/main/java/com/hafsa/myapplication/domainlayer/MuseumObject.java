package com.hafsa.myapplication.domainlayer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import com.hafsa.myapplication.modellayer.MuseumObjectDto;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.*;

@Entity(tableName = "museumObject")
public class MuseumObject implements Comparable, Serializable {

    @PrimaryKey
    @NonNull
    private String itemId;
    private String name;
    //private HashMap<String, String> pictures;
    private String brand;
    //private List<String> technicalDetails;
    //private List<Integer> timeFrame;
    private String description;
    private int year;
    //private List<String> categories;
    private boolean working;

    private static Comparator dateComparator = new DateComparator();
    public static Comparator getDateComparator() { return dateComparator; }


//    public MuseumObject(String itemId, String name, HashMap<String, String> pictures, String brand,
//                        List<String> technicalDetails, List<Integer> timeFrame, String description, int year,
//                        List<String> categories, boolean working) {
//        this.itemId = itemId;
//        this.name = name;
//        //this.pictures = pictures;
//        this.brand = brand;
//        //this.technicalDetails = technicalDetails;
//        //this.timeFrame = timeFrame;
//        this.description = description;
//        this.year = year;
//        //this.categories = categories;
//        this.working = working;
//    }

    public MuseumObject(String itemId, String name, String brand, String description, int year, boolean working) {
        this.itemId = itemId;
        this.name = name;
        //this.pictures = pictures;
        this.brand = brand;
        //this.technicalDetails = technicalDetails;
        //this.timeFrame = timeFrame;
        this.description = description;
        this.year = year;
        //this.categories = categories;
        this.working = working;
    }

    public MuseumObject(MuseumObject o) {
        this.itemId = o.itemId;
        this.name = o.name;
        this.brand = o.brand;
        this.description = o.description;
        this.year = o.year;
        this.working = o.working;
    }

    public MuseumObject(MuseumObjectDto o) {
        this.itemId = o.getItemId();
        this.name = o.getName();
        this.brand = o.getBrand();
        this.description = o.getDescription();
        this.year = o.getYear();
        this.working = o.getWorking();
    }

    @Override
    public int compareTo(Object o) {
        return this.name.compareTo(((MuseumObject)o).name);
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public HashMap<String, String> getPictures() {
//        return pictures;
//    }
//
//    public void setPictures(HashMap<String, String> pictures) {
//        this.pictures = pictures;
//    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

//    public List<String> getTechnicalDetails() {
//        return technicalDetails;
//    }
//
//    public void setTechnicalDetails(List<String> technicalDetails) {
//        this.technicalDetails = technicalDetails;
//    }
//
//    public List<Integer> getTimeFrame() {
//        return timeFrame;
//    }
//
//    public void setTimeFrame(List<Integer> timeFrame) {
//        this.timeFrame = timeFrame;
//    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

//    public List<String> getCategories() {
//        return categories;
//    }
//
//    public void setCategories(List<String> categories) {
//        this.categories = categories;
//    }

    public boolean getWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }



}
