package com.hafsa.myapplication.domainlayer;

import java.util.Comparator;
import com.hafsa.myapplication.domainlayer.MuseumObject;

public class DateComparator implements Comparator<MuseumObject> {

    @Override
    public int compare(MuseumObject museumObject, MuseumObject t1) {
        if (museumObject.getYear() == t1.getYear()) {
            return 0;
        }
        else if (museumObject.getYear() > t1.getYear()) {
            return 1;
        }
        else {
            return -1;
        }
    }
}
