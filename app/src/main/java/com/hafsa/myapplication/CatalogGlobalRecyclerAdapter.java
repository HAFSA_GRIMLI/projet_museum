package com.hafsa.myapplication;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.hafsa.myapplication.databinding.CatalogGlobalRecyclerViewLayout1Binding;
import com.hafsa.myapplication.databinding.CatalogGlobalRecyclerViewLayout2Binding;
import com.hafsa.myapplication.domainlayer.MuseumObject;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.databinding.DataBindingUtil;

public class CatalogGlobalRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MuseumObject> museumObjectList;

    private int style = 0;

    @Override
    public int getItemViewType(int position) {
        return position % 2 * 2;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parentViewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch(viewType) {
            case 0:
                viewHolder = new ViewHolderType1(
                    DataBindingUtil.inflate(
                            LayoutInflater.from(parentViewGroup.getContext()),
                            R.layout.catalog_global_recycler_view_layout_1,
                            parentViewGroup, false
                    ));
                break;
            case 2:
                viewHolder = new ViewHolderType2(
                    DataBindingUtil.inflate(
                            LayoutInflater.from(parentViewGroup.getContext()),
                            R.layout.catalog_global_recycler_view_layout_2,
                            parentViewGroup, false
                    ));
                break;
            default:
                return viewHolder;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        MuseumObject currentObject = museumObjectList.get(position);
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderType1 viewHolderType1 = (ViewHolderType1) holder;
                viewHolderType1.viewDataBinding.setMuseumObject(currentObject);
                break;
            case 2:
                ViewHolderType2 viewHolderType2 = (ViewHolderType2) holder;
                viewHolderType2.viewDataBinding.setMuseumObject(currentObject);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return (museumObjectList != null) ? museumObjectList.size() : 0 ;
    }

    public void setMuseumObjectList(ArrayList<MuseumObject> museumObjectList) {
        this.museumObjectList = museumObjectList;
        notifyDataSetChanged();
    }

    private void reverseList() {
        List<MuseumObject> museumObjectListReversed = new ArrayList<MuseumObject>();
        for (int i = museumObjectList.size() -1; i >= 0; i--) {
            museumObjectListReversed.add(museumObjectList.get(i));
        }
        museumObjectList = museumObjectListReversed;
    }

    public void sortAlphabeticAsc() {
        Collections.sort(museumObjectList);
        notifyDataSetChanged();
    }

    public void sortAlphabeticDes() {
        Collections.sort(museumObjectList);
        reverseList();
        notifyDataSetChanged();
    }

    public void sortDateAsc() {
        Collections.sort(museumObjectList, MuseumObject.getDateComparator());
        notifyDataSetChanged();
    }

    public void sortDateDes() {
        Collections.sort(museumObjectList, MuseumObject.getDateComparator());
        reverseList();
        notifyDataSetChanged();
    }

    class ViewHolderType1 extends RecyclerView.ViewHolder {
        private CatalogGlobalRecyclerViewLayout1Binding viewDataBinding;

        public ViewHolderType1(CatalogGlobalRecyclerViewLayout1Binding viewLayout1Binding) {
            super(viewLayout1Binding.getRoot());
            this.viewDataBinding = viewLayout1Binding;
            viewLayout1Binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("itemId", viewLayout1Binding.getMuseumObject().getItemId());
                    NavController c = Navigation.findNavController(view);
                    c.navigate(R.id.action_CatalogueFragment_to_DetailObjectFragment, bundle);
                }
            });
        }
    }

    class ViewHolderType2 extends RecyclerView.ViewHolder {
        private CatalogGlobalRecyclerViewLayout2Binding viewDataBinding;

        public ViewHolderType2(CatalogGlobalRecyclerViewLayout2Binding viewLayout2Binding) {
            super(viewLayout2Binding.getRoot());
            this.viewDataBinding = viewLayout2Binding;
            viewLayout2Binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("itemId", viewLayout2Binding.getMuseumObject().getItemId());
                    NavController c = Navigation.findNavController(view);
                    c.navigate(R.id.action_CatalogueFragment_to_DetailObjectFragment, bundle);
                }
            });
        }
    }
}