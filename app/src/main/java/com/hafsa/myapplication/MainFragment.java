package com.hafsa.myapplication;

import android.os.Bundle;
import android.view.*;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

public class MainFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // inflate the layout for this fragment
        return inflater.inflate(R.layout.welcome_fragment, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        //view.findViewById(R.id.enter_museum_button).setOnClickListener();
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.enter_museum_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                //Fragment navhost = getSupportFragmentManager().findFragmentById(R.id.CatalogueFragment);
                NavController c = NavHostFragment.findNavController(MainFragment.this);
                c.navigate(R.id.action_WelcomeFragment_to_CatalogueFragment, bundle);
            }
        });
    }

    /*
    view.findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
6 @Override
7 public void onClick(View view) {
8 if (!TextUtils.isEmpty(editTextLastName.getText()) && !TextUtils.isEmpty(editTextLastName.getText()))
{
9 Bundle bundle = new Bundle();
10 bundle.putString(”lastName”, editTextLastName.getText().toString());
11 bundle.putString(”firstName”, editTextFirstName.getText().toString());
12 NavHostFragment.findNavController(FirstFragment.this)
13 .navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);
14 }
15 }
16 });
     */
}
