package com.hafsa.myapplication;

import android.os.Bundle;
import android.view.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.hafsa.myapplication.domainlayer.CatalogueViewModel;
import com.hafsa.myapplication.domainlayer.MuseumObject;

import java.util.ArrayList;
import java.util.List;

public class CatalogueFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CatalogGlobalRecyclerAdapter adapter;
    private CatalogueViewModel viewModel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_catalogue, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.catalogueGlobal_recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CatalogGlobalRecyclerAdapter();
        recyclerView.setAdapter(adapter);

        viewModel = new ViewModelProvider(requireActivity()).get(CatalogueViewModel.class);
        getAllMuseumObject();
    }

    private void getAllMuseumObject() {
        viewModel.getResultMuseumObject().observe(this, new Observer<List<MuseumObject>>() {
            @Override
            public void onChanged(@Nullable List<MuseumObject> museumObjectList) {
                adapter.setMuseumObjectList((ArrayList<MuseumObject>) museumObjectList);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_catalog, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_sort_alpha_asc:
                adapter.sortAlphabeticAsc();
                return true;
            case R.id.action_sort_alpha_dsc:
                adapter.sortAlphabeticDes();
                return true;
            case R.id.action_sort_by_date_asc:
                adapter.sortDateAsc();
                return true;
            case R.id.action_sort_by_date_dsc:
                adapter.sortDateDes();
                return true;
            case R.id.action_sort_by_popularity_asc:
                /// TODO call a function
                return true;
            case R.id.action_sort_by_popularity_dsc:
                /// TODO call a function
                return true;
            case R.id.action_reset_filter:
                getAllMuseumObject();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
