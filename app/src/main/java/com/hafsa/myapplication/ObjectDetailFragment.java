package com.hafsa.myapplication;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import com.hafsa.myapplication.databinding.FragmentObjetDetailBinding;
import com.hafsa.myapplication.domainlayer.CatalogueViewModel;
import com.hafsa.myapplication.domainlayer.MuseumObject;
import com.hafsa.myapplication.domainlayer.ObjectDetailViewModel;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ObjectDetailFragment extends Fragment {

    private ObjectDetailViewModel viewModel;
    private MuseumObject museumObject;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_objet_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(requireActivity()).get(ObjectDetailViewModel.class);

        String itemId = getArguments().getString("itemId");
        getObjectById(itemId);


    }

    public MuseumObject getMuseumObject() {
        return museumObject;
    }

    private void setMuseumObject(MuseumObject o) {
        this.museumObject = o;
    }

    private void getObjectById(String id) {
        viewModel.getResultMuseumObject(id).observe(this, new Observer<List<MuseumObject>>() {
            @Override
            public void onChanged(List<MuseumObject> museumObjectList) {
                if (museumObjectList != null && !museumObjectList.isEmpty()) {
                    MuseumObject o = museumObjectList.get(0);
                    setMuseumObject(o);
                }
            }
        });
    }

}
